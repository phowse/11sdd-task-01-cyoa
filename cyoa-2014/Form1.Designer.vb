﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.txtStory = New System.Windows.Forms.TextBox()
        Me.cmdOption1 = New System.Windows.Forms.Button()
        Me.cmdOption2 = New System.Windows.Forms.Button()
        Me.cmdOption3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtTitle
        '
        Me.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTitle.Location = New System.Drawing.Point(12, 12)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.ReadOnly = True
        Me.txtTitle.Size = New System.Drawing.Size(393, 13)
        Me.txtTitle.TabIndex = 0
        Me.txtTitle.TabStop = False
        '
        'txtStory
        '
        Me.txtStory.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txtStory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStory.Location = New System.Drawing.Point(12, 38)
        Me.txtStory.Multiline = True
        Me.txtStory.Name = "txtStory"
        Me.txtStory.ReadOnly = True
        Me.txtStory.Size = New System.Drawing.Size(393, 142)
        Me.txtStory.TabIndex = 1
        Me.txtStory.TabStop = False
        '
        'cmdOption1
        '
        Me.cmdOption1.Location = New System.Drawing.Point(12, 186)
        Me.cmdOption1.Name = "cmdOption1"
        Me.cmdOption1.Size = New System.Drawing.Size(393, 53)
        Me.cmdOption1.TabIndex = 2
        Me.cmdOption1.Text = "Button1"
        Me.cmdOption1.UseVisualStyleBackColor = True
        '
        'cmdOption2
        '
        Me.cmdOption2.Location = New System.Drawing.Point(12, 244)
        Me.cmdOption2.Name = "cmdOption2"
        Me.cmdOption2.Size = New System.Drawing.Size(393, 53)
        Me.cmdOption2.TabIndex = 3
        Me.cmdOption2.Text = "Button2"
        Me.cmdOption2.UseVisualStyleBackColor = True
        '
        'cmdOption3
        '
        Me.cmdOption3.Location = New System.Drawing.Point(12, 302)
        Me.cmdOption3.Name = "cmdOption3"
        Me.cmdOption3.Size = New System.Drawing.Size(393, 53)
        Me.cmdOption3.TabIndex = 4
        Me.cmdOption3.Text = "Button3"
        Me.cmdOption3.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(417, 367)
        Me.Controls.Add(Me.cmdOption3)
        Me.Controls.Add(Me.cmdOption2)
        Me.Controls.Add(Me.cmdOption1)
        Me.Controls.Add(Me.txtStory)
        Me.Controls.Add(Me.txtTitle)
        Me.Name = "frmMain"
        Me.Text = "Our awesome adventure"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents txtStory As System.Windows.Forms.TextBox
    Friend WithEvents cmdOption1 As System.Windows.Forms.Button
    Friend WithEvents cmdOption2 As System.Windows.Forms.Button
    Friend WithEvents cmdOption3 As System.Windows.Forms.Button

End Class
