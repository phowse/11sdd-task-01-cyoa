﻿Imports System.IO

Public Class frmMain

    ' Localise our file paths
    Dim filepath As String = Application.StartupPath & "\text\"
    Dim cyoa As String = filepath & "cyoa.txt"

    ' A structure is much like an object. It can be used to collect
    ' data and methods together under one variable name. Here, we 
    ' create a structure called "room" to store all the details
    ' about the location in our story.
    Structure room
        Dim roomID As Integer
        Dim title As String
        Dim story As String
        Dim opt1Text As String
        Dim opt1Target As Integer
        Dim opt2Text As String
        Dim opt2Target As Integer
        Dim opt3Text As String
        Dim opt3Target As Integer
    End Structure

    ' We create an array of room data types to store all our locations
    ' in the file
    Dim arrRooms(1) As room

    ' It is good practice to keep our mainline clear. Thus, we create subs 
    ' to initialise the application
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        setUp()
    End Sub

    Private Sub setUp()
        loadFile()          ' load the file into our array of rooms
        loadRoom(0)         ' load the first room
    End Sub

    Private Sub loadFile()

        Dim lineNum As Integer = 0                  ' keep track of which line we are reading in

        ' Try statements are used to prevent programs from crashing. 
        ' The program attempts to execute the section of code in a 
        ' sandbox first. Any errors can then be caught by the code
        ' first, before crashing the entire program
        Try
            Using sr As New StreamReader(cyoa)          ' We create a stream reader object for our file
                While Not sr.EndOfStream                ' While we have lines to read in
                    Dim line As String
                    line = sr.ReadLine()                ' Read a line in one at a time
                    writeRooms(line, lineNum)           ' Send the line out to be parsed into our array of rooms
                    lineNum += 1
                End While
            End Using
        Catch e As Exception                            ' Report any errors in reading the line of code
            Dim errMsg As String = "Problems: " & e.Message
            MsgBox(errMsg)
        End Try
    End Sub

    Private Sub writeRooms(line As String, id As Integer)

        Dim tmp()

        ' Split is a function that divides a string into an array based
        ' on a delimiting character
        tmp = Split(line, "|")

        arrRooms(id).roomID = Int(tmp(0))
        arrRooms(id).title = tmp(1)
        arrRooms(id).story = tmp(2)
        arrRooms(id).opt1Text = tmp(3)
        arrRooms(id).opt1Target = tmp(4)
        arrRooms(id).opt2Text = tmp(5)
        arrRooms(id).opt2Target = tmp(6)
        arrRooms(id).opt3Text = tmp(7)
        arrRooms(id).opt3Target = tmp(8)

    End Sub

    Private Sub loadRoom(roomID As Integer)

        ' Loads the room given as a parameter to this sub-routine

        txtTitle.Text = arrRooms(roomID).title
        txtStory.Text = arrRooms(roomID).story
        cmdOption1.Text = arrRooms(roomID).opt1Text
        cmdOption1.Tag = arrRooms(roomID).opt1Target
        cmdOption2.Text = arrRooms(roomID).opt2Text
        cmdOption2.Tag = arrRooms(roomID).opt2Target
        cmdOption3.Text = arrRooms(roomID).opt3Text
        cmdOption3.Tag = arrRooms(roomID).opt3Target
    End Sub

    Private Sub moveRoom(sender As Object, e As EventArgs) Handles cmdOption1.Click, cmdOption2.Click, cmdOption3.Click
        ' We store the target location for a button click in its 
        ' .tag property. We can then easily load up any room that 
        ' we want to.
        loadRoom(Int(sender.tag))
    End Sub

End Class
